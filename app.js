const express = require("express");
const app = express();

app.set('view engine', 'ejs');

app.get('/', (req, res) => {
    res.render('index', { title: 'Home' });
})

app.get("/login", (req, res) => {
    res.render('login', { title: 'Login' });
})

app.get("/register", (req, res) => {
    res.render('register', { title: 'Register' });
})

app.get("/category", (req, res) => {
    res.render('category', { title: 'Category' });
})

app.get("/book", (req, res) => {
    res.render('book', { title: 'Detail Book' });
})

app.get("/cart", (req, res) => {
    res.render('cart', { title: 'Cart' });
})

app.listen(8000, () => {
    console.log("Server running on port 8000");
});

console.log("OK!");